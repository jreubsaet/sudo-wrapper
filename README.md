# sudo Wrapper
This script is meant as a wrapper around **su** to provide a **sudo**-like interface for applications or scripts that hardcode sudo to elevate privileges.  It can also be used as a stand-alone replacement for general use - it is however not a drop-in replacement for **sudo**, as some options are either not implemented, or implemented differently.  Basic **sudo** usage should work fine, however.
See `sudo --manual` for more information.

This wrapper has been checked using shellcheck to remove any inconsistencies, as well as Bashisms.  This should make it
POSIX-compliant, and as such should run properly under most (if not all) POSIX-compliant shells.

# Installation

### Manual Installation

#### Script
1. Place **sudo** in `/usr/bin` (or `/usr/local/bin` if you have the `sudo` package installed).
2. Then, apply the right permissions using `chmod 755 /path/to/sudo` (e.g. `chmod 755 /usr/local/bin/sudo`).
* Alternatively, place it in a directory that is in your path and is sought in before `/usr/bin`, and set the permissions accordingly.

#### Manual Page
1. Compress **sudo-wrapper.man** using `gzip`, and name it **sudo-wrapper.1.gz** (e.g. `gzip sudo-wrapper.man -c > sudo-wrapper.1.gz`).
2. Copy the gzip'd manual page to `/usr/local/man/man1`.
3. Apply the right permissions using `chmod 644 /usr/local/man/man1/sudo-wrapper.1.gz`.

### Package Manager
For users of Arch-based distributions, I have included a PKGBUILD.  Build the package using makepkg and install it.

## TODO
* 'Convert' UID/GIDs to user / group names to emulate **sudo**'s ID feature.
* Further mprove compatibility with **sudo** by properly parsing options with an '='.

## FIXED / ADDED / WONTFIX
* FIXED: ~~Properly read arrays to provide multiple supplemental groups.~~ Fixed by having the user supply a comma-separated list and parse it through a for-loop.
* WONTFIX: ~~Implement a `sudoedit` / `sudo -e` type function.~~ Arrays are not (really) supported under the POSIX shell.
* WONTFIX: ~~When piping commands, only pipe the commands to be run as the substitute user, not the entire command chain.~~ This does seem to be the default behavior in any tested case.
